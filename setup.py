#!/usr/bin/env python

"""
Installer for ycp
"""

from distutils.core import setup

setup(
    name='ycp',
    description='ycp',
    author='Richard Peng',
    author_email='richardpeng@kkbox.com',
    version='0.0.1',
    url='https://bitbucket.org/ycpenx/ycp',
    packages=['ycp'],
    package_dir={'ycp': 'ycp'}
)
